#!/usr/env python
# -*- coding: utf-8 -*-

def is_prime(num):
    """This function checks if given number is a prime one
    """
    for i in xrange(2, num):
        if (num % i) == 0:
            # not a prime number
            return False
    return True

def one_prime():
    """This function returns a generator which yields one prime number on every
next() usage against it"""
    num = 2
    while True:
        if is_prime(num):
            yield num
        num += 1

def ten_primes():
    """This function returns a generator which yields 10 prime numbers on every
next() usage against it"""
    prime_gen = one_prime()
    while True:
        result = []
        for _ in xrange(10):
            result.append(next(prime_gen))
        yield result

if __name__ == "__main__":
    i = 0
    for number in ten_primes():
        print(number)
        i += 1
        if i == 5:
            break
